import erppeek

client = erppeek.Client.from_config('odoo')

ShareTemplate = client.model("product.template")
Partner = client.model("res.partner")
ShareLine = client.model("share.line")

share_template = ShareTemplate.browse(
    [
        ("is_share", "=", True),
    ],
    limit=1
)

share_product, = share_template.product_variant_id

cooperator_partners = Partner.browse(
    [
        ("sc_cooperator_register_number", "!=", None),
        ("sc_cooperator_register_number", "!=", 0),
        ("sc_cooperator_end_date", "=", None),
        ("cooperator_register_number", "=", None),
    ]
)


print("* Membership:")
for partner in cooperator_partners:
    member_vals = {
        "member": True,
        "old_member": False,
        "cooperator_register_number": int(partner.sc_cooperator_register_number)
    }
    share_line_vals = {
        "share_number": 1,
        "share_product_id": int(share_product.id),
        "partner_id": partner.id,
        "share_unit_price": int(share_product.list_price),
        "effective_date": partner.sc_effective_date
    }
    partner.write(
        member_vals
    )
    ShareLine.create(
        share_line_vals
    )
    print("Cooperator modificado {}".format(partner.id))
