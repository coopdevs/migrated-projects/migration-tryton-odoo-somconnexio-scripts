import erppeek

import csv

client = erppeek.Client.from_config('odoo')

with open('adsl_contract_info_to_update.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        print("Processing line: ", row['ref'])
        contract = client.model('contract.contract').browse([('code', '=', row['ref'])])

        if not contract:
            print("This contract is not in the database. ", row['ref'])
            continue
        
        contract_args = {
            'id_order': row['id_order'],
            'ppp_password': row['ppp_password'],
            'administrative_number': row['administrative_number']
        }
        if row['loaded_ppp_user'].lower() == 'x':
            contract_args['ppp_user'] = row['ppp_user']

        contract.adsl_service_contract_info_id.write(contract_args)

        print("Finished processing line: ", row['ref'])
