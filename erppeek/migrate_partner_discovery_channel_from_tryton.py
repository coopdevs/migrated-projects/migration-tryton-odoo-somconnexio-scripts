import erppeek

import csv

client = erppeek.Client.from_config('odoo')

discovery_channels = client.model('discovery.channel').browse([('id', '>', 0)])
discovery_channels_dict = {}
for ds in discovery_channels:
    name = ds.name
    if name == "Cercadors d'Internet (Google, Yahoo, etc)":
        name = 'Cercadors d´Internet (google, yahoo, etc)'
    elif name == 'Mitjans de comunicació tradicionals (premsa, ràdio, televisió, etc)':
        name = 'Mitjans de comunicació tradicionals (premsa, ràdio, televisió)'
    elif name == 'Xarxes socials (Facebook, Twitter, Instagram, Linkedin, etc)':
        name = 'Xarxes socials (Facebook, Twitter, Instagram, Linkedin,etc)'

    discovery_channels_dict.update({
        name: ds.id,
    })

processed = 0
not_found = 0
with open('tryton_partners_discovery_channel.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        print("Processing line with partner: ", row['partner'])
        partner = client.model('res.partner').browse([('ref', '=', row['partner'])])

        if not partner:
            print("Partner {} not found in the database.".format(row['partner']))
            not_found += 1
            continue

        # We need to select only the first element of the RecordList to check if the
        # discovery channel is set.
        if partner.discovery_channel_id[0]:
            print("Partner {} has discovery_channel".format(row['partner']))
            continue

        partner_args = {
            'discovery_channel_id': discovery_channels_dict[row['name']],
        }
        processed += 1
        partner.write(partner_args)

        print("Finished processing line with partner: {} associated witn DC: {}".format(row['partner'], row['name']))

print("Not found partners {}\nProcessed partners {}".format(not_found, processed))
