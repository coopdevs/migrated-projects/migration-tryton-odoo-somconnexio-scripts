import erppeek

client = erppeek.Client.from_config('odoo')

Partner = client.model("res.partner")

partners = Partner.browse(
    [
        ("effective_date", "!=", None)
    ],
    order="id",
)

for partner in partners:
    partner.write({
        "share_ids": partner.share_ids
    })
    print("Recomputed effective date for partner ID {}".format(partner.id))
