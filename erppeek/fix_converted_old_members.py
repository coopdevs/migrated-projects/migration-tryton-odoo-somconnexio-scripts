import erppeek

import csv

client = erppeek.Client.from_config('odoo')

ShareTemplate = client.model("product.template")
Partner = client.model("res.partner")
ShareLine = client.model("share.line")
SubscriptionRegister = client.model("subscription.register")
IrSequence = client.model("ir.sequence")

register_operation_sequence = IrSequence.browse(
    [
        ("code", "=", "register.operation"),
    ],
    limit=1
)


partners_to_update = []
with open('old_members_converted.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        partners_to_update.append(row)

share_template = ShareTemplate.browse(
    [
        ("is_share", "=", True),
    ],
    limit=1
)

share_product, = share_template.product_variant_id
share_product_list_price = share_product.list_price
share_product_id = share_product.id

for partner in partners_to_update:
    partner_id = int(partner.get("id"))
    effective_date = partner.get("effective_date")

    cooperator_partner = Partner.browse(partner_id)

    cooperator_partner.write({
        "member": True,
        "old_member": False,
        "coop_candidate": False,
    })
    ShareLine.create({
        "share_number": 1,
        "share_product_id": int(share_product_id),
        "partner_id": partner_id,
        "share_unit_price": int(share_product_list_price),
        "effective_date": effective_date
    })

    register_number_operation = register_operation_sequence.next_by_id()
    SubscriptionRegister.create({
        "partner_id": partner_id,
        "quantity": 1,
        "share_product_id": int(share_product_id),
        "share_unit_price": int(share_product_list_price),
        "date": effective_date,
        "type": "subscription",
        "name": register_number_operation,
        "register_number_operation": int(register_number_operation)
    })
    print("Cooperator modificado {}".format(partner_id))
