from activity_odoo_res_id_get import (
    get_codes_ids_dict, read_rows, write_rows_with_odoo_ids
)


rows = read_rows('../data/mail.activity-contract.csv')
contract_codes_ids = get_codes_ids_dict(rows, 'contract.contract', 'code')
write_rows_with_odoo_ids(rows, contract_codes_ids, '../data/mail.activity-contract_Odoo.csv')
