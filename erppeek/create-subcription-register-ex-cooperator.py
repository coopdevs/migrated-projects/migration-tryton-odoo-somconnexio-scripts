import erppeek

client = erppeek.Client.from_config('odoo')

ShareTemplate = client.model("product.template")
Partner = client.model("res.partner")
ShareLine = client.model("share.line")
SubscriptionRegister = client.model("subscription.register")
IrSequence = client.model("ir.sequence")

sequence_operation = IrSequence.browse(
    [
        ("code", "=", "register.operation"),
    ],
    limit=1
)

share_template = ShareTemplate.browse(
    [
        ("is_share", "=", True),
    ],
    limit=1
)

share_product, = share_template.product_variant_id

cooperator_partners = Partner.browse(
    [
        ("member", "=", False),
        ("sc_cooperator_end_date", "!=", None),
    ]
)


print("Subscription Register migration for old members...")
for partner in cooperator_partners:
    cooperator_partners = SubscriptionRegister.browse(
        [
            ("partner_id", "=", partner.id),
            ("type", "=", "sell_back"),
            ("date", "=", partner.sc_cooperator_end_date),
        ]
    )
    if cooperator_partners:
        print("Partner {} already migrated".format(partner.id))
        continue
    sub_reg_operation = sequence_operation.next_by_id()
    subs_register_vals = {
        "name": sub_reg_operation,
        "register_number_operation": int(sub_reg_operation),
        "share_product_id": int(share_product.id),
        "share_unit_price": share_template.list_price[0],
        "partner_id": partner.id,
        "date": partner.sc_cooperator_end_date,
        "quantity": 1,
        "type": "sell_back",
    }
    SubscriptionRegister.create(
        subs_register_vals
    )
    Partner.write([partner.id], {"old_member": True})
    print("Sell back subscription register created for partner {} with date {}".format(
        partner.id,
        partner.sc_cooperator_end_date
    ))
