#!/usr/bin/env python
import click

from resources.configuration import Configuration


@click.command()
@click.option('--start', default=None, help="The start ID to search")
@click.option('--end', default=None, help="The end ID to search")
@click.option('--id', default=None, help="The ID to search")
@click.option('--limit', default=None, help="Limit the number of objects returned")
@click.option('--last-cooperator', default=None, help="Limit the sponsor cooperator the this ID")
@click.option('--old-sponsored-cat-id', default=None, help="The Old sponsored category ID")
@click.option('--supplier-cat-id', default=None, help="The Suppliers category ID")
@click.argument('resource', default='partner')
@click.argument('group', default='cooperator')
def extract_data(start, end, id, limit, last_cooperator, old_sponsored_cat_id, supplier_cat_id, resource, group):
    """
    Extract a CSV with all the data of resource formated to be imported in Odoo.
    The file resultat has the name of the model in Odoo.

    Valid RESOURCES:

    * partner --> All the partner data (addresses, contact methods, banks, identifiers)

        * Groups of Partners:

            - cooperator: Partners members of SC with partner number and without partner end date

            - ex-cooperator: Partners with partner number but with partner end date

            - sponsored:

                - sponsored: Partners no members but with referent

                - coop agreement (MO, MSM):

                - old-sponsored: Partners no members but with services and without referent

            - suppliers: Partners that provide services to SC

    * Activity ---> All the activities data (Summary, record, origin, dates, ... )

        * Group of Activities:

            - partner: Activities related to Contacts

            - contract: Activities related to Contracts

    """
    config = Configuration(resource, group)
    config.db_connect()

    r_class = config.resource_class
    data = r_class(
        config,
        start,
        end,
        id,
        limit,
        last_cooperator,
        group,
        old_sponsored_cat_id,
        supplier_cat_id,
    ).get_data()

    # Close communication with the database
    config.exit()

    config.write_csv(data)
