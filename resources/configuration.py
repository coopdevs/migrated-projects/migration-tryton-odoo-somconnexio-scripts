import psycopg2
import csv
import os
from configparser import ConfigParser

from resources.partner import Partner
from resources.activity import Activity


class Configuration:
    resources = {
        "partner": Partner,
        "activity": Activity,
    }

    def __init__(self, resource, group):
        config_object = ConfigParser()
        config_object.read("config.ini")
        dbinfo = config_object["DB"]

        self.db_name = dbinfo["name"]
        self.db_user = dbinfo["user"]
        self.db_port = dbinfo["port"]

        self.resource = resource
        self.resource_class = self.resources[resource]
        self.group = group

        # Create the path if not exists
        self.file_path = "./data"
        if not os.path.exists(self.file_path):
            os.makedirs(self.file_path)

    def db_connect(self):
        self.conn = psycopg2.connect(
            dbname=self.db_name,
            user=self.db_user,
            port=self.db_port,
        )
        if self.conn is not None:
            print('Connection established to PostgreSQL.')
        else:
            print('Connection not established to PostgreSQL.')
        # Open a cursor to perform database operations
        self.cur = self.conn.cursor()

    def exit(self):
        self.cur.close()
        self.conn.close()
        print('Connection closed to PostgreSQL.')

    def write_csv(self, data):
        file_path = os.sep.join([
            self.file_path,
            self.resource_class.filename.format(self.group)])
        with open(file_path, mode='w') as csv_file:
            writer = csv.DictWriter(
                csv_file,
                fieldnames=self.resource_class.csv_fields
            )

            writer.writeheader()
            for row in data:
                writer.writerow(row)

            print("File saved in {}".format(file_path))
